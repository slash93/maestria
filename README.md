<h1>Maestría en Ciberseguridad | Universidad Cenfotec </h1>

El programa de la Maestría en Ciberseguridad de la Universidad Cenfotec ofrece una preparación especializada y una base sólida en la seguridad de las tecnologías de información y comunicación, en un programa que combina experiencia,conocimiento, educación y ética.

<h1></h1>

<h2>Plan de Estudios </h2>

<h3>Primer Cuatrimestre</h3>

* Introducción a la seguridad de la información 
* Principios de la criptografía
* Tecnología de redes y telecomunicaciones seguras
* Métodos de investigación aplicada

<h3>Segundo Cuatrimestre</h3>

* Control de acceso, seguridad ambiental y física
* Criptografía aplicada
* Seguridad en redes inalámbricas y dispositivos móviles

<h3>Tercer Cuatrimestre</h3>

* Seguridad en sistemas operativos
* Aspectos culturales, éticos, legales y regulatorios
* Seguridad y protocolos de comunicación

<h3>Cuarto Cuatrimestre</h3>

* Seguridad en aplicaciones y sistemas
* Seguridad de datos almacenados
* Análisis y detección de vulnerabilidades

<h3>Quinto Cuatrimestre</h3>

* Continuidad del negocio y recuperación de desastres
* Análisis de evaluación de riesgos de seguridad
* Proyecto de investigación aplicada 1
* Curso electivo

<h3>Sexto Cuatrimestre</h3>

* Arquitectura y diseño de seguridad 
* Administración de sistema de gestión de seguridad de la información
* Proyecto de investigación aplicada 2
