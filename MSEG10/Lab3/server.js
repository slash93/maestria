// Security / API / Trusted Origins -> http://localhost:3000 [CORS + Redirect]
// Applications -> Add Application
// - Create New App (Boton) -> OpenID Connect
// -- Application Name: Cenfochat
// -- Login Redirect URIs: http://localhost:3000/authorization-code/callback
// App GENENERAL EDIT
// -- Allowed Grant Types [Implicit]
// -- Login Initiated by Either Okta or App --> (display icon to users)
// -- Save
// Applications -> Cenfochat --> [cog] --> Assign to Users
// Seleccione su usuario de okta. --> Save

"use strict";

const express = require("express");
const session = require("express-session");
const ExpressOIDC = require("@okta/oidc-middleware").ExpressOIDC;
var cons = require('consolidate');
var path = require('path');
let app = express();

// Globals
const OKTA_ISSUER_URI = "https://ucenfotec-ac-ca.okta.com/"
const OKTA_CLIENT_ID = "0oa318h1eJYWQKQRE4x6";
const OKTA_CLIENT_SECRET = "Vmg2XpqrcY7w68rcxDhLG7Yj1Yb9uT3vZpZinSvW";
const REDIRECT_URI = "http://localhost:3000/authorization-code/callback";
const PORT = process.env.PORT || "3000";
const SECRET = "hjsadfghjakshdfg87sd8f76s8d7f68s7f632342ug44gg423636346f";

// MVC View Setup
app.engine('html', cons.swig)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// App middleware
app.use("/static", express.static("static"));

app.use(session({
  cookie: { httpOnly: true },
  secret: SECRET
}));

let oidc = new ExpressOIDC({
  issuer: OKTA_ISSUER_URI,
  client_id: OKTA_CLIENT_ID,
  client_secret: OKTA_CLIENT_SECRET,
  redirect_uri: REDIRECT_URI,
  routes: { callback: { defaultRedirect: "/chat" } },
  scope: 'openid profile'
});

// App routes
app.use(oidc.router);

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/chat", oidc.ensureAuthenticated(), (req, res) => {
  res.render("chat", { user: req.userinfo });
});

app.get("/logout", (req, res) => {
  req.logout();
  res.redirect(OKTA_ISSUER_URI + "/login/signout?fromURI=" + "http://localhost:3000/");
});

const openIdClient = require('openid-client');
openIdClient.Issuer.defaultHttpOptions.timeout = 20000;
//--------------------

var http = require('http').Server(app);
var io = require('socket.io')(http);
var val = require('cenfolib');

//--------------------

oidc.on("ready", () => {
  console.log("Server running on port: " + PORT);
  http.listen(parseInt(PORT));
});

oidc.on("error", err => {
  console.error(err);
});

// escuchar una conexion por socket
io.on('connection', function(socket){
  // si se escucha "chat message"
  socket.on('Evento-Mensaje-Server', function(msg){
    console.log('hola');
    // volvemos a emitir el mismo mensaje
    msg = val.validateMessage(msg);
    io.emit('Evento-Mensaje-Server', msg);
  });
});
