// Script de Pruebas

// Libreria Cenfolib
var val = require('cenfolib');
var assert = require('assert');

// Pruebas:

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_image', function() {
    it('Debe devolver true para el PNG', function() {
      assert.equal(val.is_valid_image('https://cdn-5ce4a003f911c80f50818892.closte.com/wp-content/uploads/elementor/thumbs/php-regex-developers-okj861nm4flpjinsx9em5b4pq8ly6i2uxfclrmze6g.png'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_image', function() {
    it('Debe devolver true para JPG', function() {
      assert.equal(val.is_valid_image('https://seohackercdn-seohacker.netdna-ssl.com/wp-content/uploads/2014/06/regex-1024x480.jpg'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_image', function() {
    it('Debe devolver true para GIF', function() {
      assert.equal(val.is_valid_image('http://i.imgur.com/FxQtx.gif'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_image', function() {
    it('No soporta ZIP', function() {
      assert.equal(val.is_valid_image('http://i.imgur.com/FxQtx.zip'), false);
    });
  });
});

// prueba negativa
describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_image', function() {
    it('No soporta PHP', function() {
      assert.equal(val.is_valid_image('http://i.imgur.com/FxQtx.php'), false);
    });
  });
});

//==============================YOUTUBE===============================================

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_youtube', function() {
    it('Video valido de Youtube', function() {
      assert.equal(val.is_valid_youtube('https://www.youtube.com/watch?v=TjyXwgSzs6k'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_youtube', function() {
    it('Video valido de Youtube', function() {
      assert.equal(val.is_valid_youtube('https://www.youtube.com/watch?v=GV8zPtqOyqg'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_youtube', function() {
    it('No soporta HTTP no seguro', function() {
      assert.equal(val.is_valid_youtube('http://www.youtube.com/watch?v=GV8zPtqOyqg'), false);
    });
  });
});


describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_youtube', function() {
    it('No soporta Vimeo', function() {
      assert.equal(val.is_valid_youtube('https://vimeo.com/651235567'), false);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_youtube', function() {
    it('No soporta Vimeo', function() {
      assert.equal(val.is_valid_youtube('https://vimeo.com/149176457'), false);
    });
  });
});