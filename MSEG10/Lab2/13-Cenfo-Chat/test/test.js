// Script de Pruebas

// Libreria Cenfolib
var val = require('cenfolib');
var assert = require('assert');

// Pruebas:

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_phone', function() {
    it('Debe devolver true para +506 8297-8547', function() {
      assert.equal(val.is_valid_phone('+506 8297-8547'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_phone', function() {
    it('Debe devolver true para 8297-8547', function() {
      assert.equal(val.is_valid_phone('8297-8547'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_phone', function() {
    it('Debe devolver true para 800 800-8000', function() {
      assert.equal(val.is_valid_phone('800 800-8000'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_phone', function() {
    it('Debe devolver true para +1 800 2288-0000', function() {
      assert.equal(val.is_valid_phone('+1 800 2288-0000'), true);
    });
  });
});

// prueba negativa
describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_phone', function() {
    it('No soporta +1 (800) 2288-0000', function() {
      assert.equal(val.is_valid_phone('+1 (800) 2288-0000'), false);
    });
  });
});

// YT Videos

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_yt_video', function() {
    it('Debe devolver true para https://www.youtube.com/watch?v=z8ZqFlw6hYg', function() {
      assert.equal(val.is_valid_yt_video('https://www.youtube.com/watch?v=z8ZqFlw6hYg'), true);
    });
  });
});

// Neg
describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_yt_video', function() {
    it('Debe devolver false para https://www.youtube.com/results?search_query=slayer', function() {
      assert.equal(val.is_valid_yt_video('https://www.youtube.com/results?search_query=slayer'), false);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_yt_video', function() {
    it('Debe devolver true para http://youtu.be/Old1YzSG_S8', function() {
      assert.equal(val.is_valid_yt_video('http://youtu.be/Old1YzSG_S8'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_yt_video', function() {
    it('Debe devolver true para https://youtu.be/Old1YzSG_S8', function() {
      assert.equal(val.is_valid_yt_video('https://youtu.be/Old1YzSG_S8'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_yt_video', function() {
    it('Debe devolver true para youtu.be/Old1YzSG_S8', function() {
      assert.equal(val.is_valid_yt_video('youtu.be/Old1YzSG_S8'), true);
    });
  });
});


// Url images

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_url_image', function() {
    it('Debe devolver true para https://global-uploads.webflow.com/5ca2bcb83aece284fe8c6ada/5d9a57a9a491dddb1335ed3c_DSC02671edit.jpg', function() {
      assert.equal(val.is_valid_url_image('https://global-uploads.webflow.com/5ca2bcb83aece284fe8c6ada/5d9a57a9a491dddb1335ed3c_DSC02671edit.jpg'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_url_image usando parametros', function() {
    it('Debe devolver true para https://i.stack.imgur.com/ShCrU.jpg?s=328&g=1', function() {
      assert.equal(val.is_valid_url_image('https://i.stack.imgur.com/ShCrU.jpg?s=328&g=1'), true);
    });
  });
});


describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_url_image', function() {
    it('Debe devolver true para https://global-uploads.webflow.com/5ca2bcb83aece284fe8c6ada/5ddeaf2d731389675771c791_EC20COUNCIL.png', function() {
      assert.equal(val.is_valid_url_image('https://global-uploads.webflow.com/5ca2bcb83aece284fe8c6ada/5ddeaf2d731389675771c791_EC20COUNCIL.png'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_url_image', function() {
    it('Debe devolver true para https://media2.giphy.com/media/8plT87cI4F2gw/giphy.gif', function() {
      assert.equal(val.is_valid_url_image('https://media2.giphy.com/media/8plT87cI4F2gw/giphy.gif'), true);
    });
  });
});

describe('modulo:cenfolib', function() {
  describe('metodo:is_valid_url_image', function() {
    it('Debe devolver false para https://media2.giphy.com/test.exe', function() {
      assert.equal(val.is_valid_url_image('https://media2.giphy.com/test.exe'), false);
    });
  });
});


//getYTVideoId

describe('modulo:cenfolib', function() {
  describe('metodo:getYTVideoId', function() {
    it("Debe devolver 'Old1YzSG_S8' para http://youtu.be/Old1YzSG_S8", function() {
      assert.equal(val.getYTVideoId('http://youtu.be/Old1YzSG_S8'), 'Old1YzSG_S8');
    });
  });
});

//https://www.youtube.com/watch?v=z8ZqFlw6hYg

describe('modulo:cenfolib', function() {
  describe('metodo:getYTVideoId', function() {
    it("Debe devolver 'z8ZqFlw6hYg' para https://www.youtube.com/watch?v=z8ZqFlw6hYg", function() {
      assert.equal(val.getYTVideoId('https://www.youtube.com/watch?v=z8ZqFlw6hYg'), 'z8ZqFlw6hYg');
    });
  });
});