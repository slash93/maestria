var events = require("events");

var eventEmitter = new events.EventEmitter;

var eventA = function (){
    console.log("Event A");
}

var eventB = function (){
    console.log("Event B");
}

eventEmitter.on('connection',eventB);
eventEmitter.addListener('connection',eventA);

var eventListeners = events.EventEmitter.listenerCount(eventEmitter, 'connection');
console.log(eventListeners + " listeners for event connection");

eventEmitter.emit("connection");

eventEmitter.removeListener('connection',eventA);

console.log("Event A removed...");

eventListeners = events.EventEmitter.listenerCount(eventEmitter, 'connection');
console.log(eventListeners + " listeners for event connection");

console.log("Fin del programa");