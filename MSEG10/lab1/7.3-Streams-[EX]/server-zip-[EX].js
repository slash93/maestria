// npm install archiver --save

var fs = require('fs');
var archiver = require('archiver');

// definir el .zip
var output = fs.createWriteStream(__dirname + '/cenfo.zip');

// caracteristicas del zip
var archive = archiver('zip', {
  zlib: { level: 9 }
});

// establecer el pipe entre el output y el objeto archiver
archive.pipe(output);

// lectura del archivo a comprimit
var file1 = __dirname + '/input.txt';
archive.append(fs.createReadStream(file1), { name: 'mi-archivo.txt' });

archive.finalize();

// Ejercicio:
// Comprima todos los archivos .txt que estan en la carpeta mis-archivos

output = fs.createWriteStream('mis-archivos.zip');
archive = archiver('zip');

output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
});

archive.on('error', function(err){
    throw err;
});

archive.pipe(output);
archive.directory('mis-archivos', true, { date: new Date() });

// Listo!
archive.finalize();
