var events = require("events");

var eventEmitter = new events.EventEmitter;

var connectionHandler = function (){
    console.log("Conexion iniciada");

    eventEmitter.emit("connection_executed");
}

eventEmitter.on('connection',connectionHandler);

eventEmitter.on('connection_executed',function(){
    console.log("Conexion exitosa!")
});

eventEmitter.emit("connection");

console.log("Fin del programa")