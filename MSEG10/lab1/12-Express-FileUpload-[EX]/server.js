// librerias
var express = require('express');
var fs = require('fs');
// variable que controla Express
var app = express();
var upload = require('express-fileupload');
app.use(upload());

// GET
app.get('/', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
})

// POST
app.post('/file_upload',  function (req, res) {

  // Investigue como hacer un file upload por post
  // busque en NPM
  var dir = "./uploads/";
  if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
  }
  if(req.files){
    console.log(req.files);
    var file = req.files.archivo,
    fileName = file.name;
    file.mv(dir+fileName,function(err){
      if(err){
        console.log(err);
        res.send("ERROR!");
      }
      else{
        res.send("Done!");
      }
    })
  }

});


// Inicia el server...
var server = app.listen(8081, function () {
  console.log("Aplicacion activa en 8081");

});
